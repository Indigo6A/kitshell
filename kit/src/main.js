import Vue from "vue";
import App from "./App.vue";
import vuetify from "./plugins/vuetify";
import router from "./router";
import VueRouter from "vue-router";
Vue.config.productionTip = false;
Vue.use(VueRouter);
console.log("int..")
if(window['NL_OS']) {
window.Neutralino.init();
}
else {
  console.log("not Neutralino env")
}
new Vue({
  vuetify,
  router,
  render: (h) => h(App),
}).$mount("#app");
