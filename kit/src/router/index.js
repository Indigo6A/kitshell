import Vue from 'vue'
import VueRouter from 'vue-router'
import IpAddress  from '@/views/network/IpAddress'
import PortKill from '@/views/network/PortKill'


Vue.use(VueRouter)

const routes = [
    {
        path:'/network/IpAddress',
        component:IpAddress
    },
    {
        path:'/network/PortKill',
        component:PortKill
    }
]
const router = new VueRouter({
    routes
})
export default router