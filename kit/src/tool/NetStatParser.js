console.log("env:",process.env.NODE_ENV)
// test env don't use window
export const OS = process.env.NODE_ENV!="production"?'browsertest':window['NL_OS'];
console.log("os:"+OS)
/**
 * [Object: null prototype] {
      proto: 'tcp6',
      recvQ: '0',
      sendQ: '0',
      localAddress: ':',
      localPort: '3689',
      foreignAddress: '::',
      foreignPort: '*',
      state: 'LISTEN',
      pid: '4006',
      programName: 'rhythmbox'
    }
 * @param {*} result 
 */
export function parseLinuxNetstat(result) {
    const netstat_Tulpn =/(?<proto>\w+)\s+(?<recvQ>\d+)\s+(?<sendQ>\d+)\s+(?<localAddress>(\d|\.|:)+):(?<localPort>\d+)\s+(?<foreignAddress>(\d|\.|:)+):(?<foreignPort>\*)\s+(?<state>\w+)\s+(?<pid>\d+)\/(?<programName>(\w|\.|\/)+)/g 
    let matchResult = [];
    let temp;
    while((temp=netstat_Tulpn.exec(result))!==null)
    {
    matchResult.push(temp.groups)
    }
    return matchResult

}
async function getLinuxNetStat(port) {
  console.log("start exec :")
  let clipboardText = await Neutralino.clipboard.readText();
  console.log(`Text: ${clipboardText}`);
  try {
  let result = await Neutralino.os.execCommand("netstat -tulpn")
  console.log(result)
  let output = result.stdOut;
  console.log(result)
  return parseLinuxNetstat(output)
  }
  catch(err) {
    console.log("err:",err)
    return [];
  }
}
/**
 * 暂时只解析TCP LISTENING的
 * @param {}} result 
 * @returns 
 */
export function parseWindowsStat(result) {
  const netstat_Tulpn = /TCP\s+(?<localAddress>(\d|\.)+):(?<localPort>\d+)\s+(?<foreignAddress>(\d|\.)+):(?<foreignPort>\d)+\s+LISTENING\s+(?<pid>\d+)/g
  let matchResult = [];
  let temp;
  while((temp=netstat_Tulpn.exec(result))!==null)
  {
  matchResult.push(temp.groups)
  }
  return matchResult
}
async function getWindowsNetStat(port) {
  console.log("start exec :")
  let clipboardText = await Neutralino.clipboard.readText();
  console.log(`Text: ${clipboardText}`);
  try {
    let result = await Neutralino.os.execCommand('netstat -ano|findstr "LISTENING"')
    console.log(result)
    let output = result.stdOut;
    console.log(result.stdOut)
    return parseWindowsStat(output)
    }
    catch(err) {
      console.log("err:",err)
      return [];
    }
}
async function killTaskLinux(pid) {
  let result = await Neutralino.os.execCommand(`kill ${pid}`)
  console.log("exec result:",result);
  return result.exitCode;
}
async function killTaskWindows(pid) {
  console.log("kill windows;"+`taskkill /F /PID ${pid}|findstr "LISTENING"`)
  let result = await Neutralino.os.execCommand(`taskkill /F /PID ${pid}|findstr "LISTENING"`,{background:true})
  console.log("exec result:",result);
  return result.exitCode;
}
// For demo propose
function getH5NetStat(port) {
  let commandResult = `激活Internet连接 (仅服务器)
  Proto Recv-Q Send-Q Local Address           Foreign Address         State       PID/Program name    
  tcp        0      0 0.0.0.0:8080            0.0.0.0:*               LISTEN      5958/node           
  tcp        0      0 0.0.0.0:9330            0.0.0.0:*               LISTEN      2246/./speedify     
  tcp        0      0 127.0.0.1:5939          0.0.0.0:*               LISTEN      2278/teamviewerd    
  tcp        0      0 127.0.0.53:53           0.0.0.0:*               LISTEN      1498/systemd-resolv 
  tcp        0      0 0.0.0.0:22              0.0.0.0:*               LISTEN      2103/sshd: /usr/sbi 
  tcp        0      0 127.0.0.1:631           0.0.0.0:*               LISTEN      2532/cupsd          
  tcp        0      0 0.0.0.0:8000            0.0.0.0:*               LISTEN      3066/docker-proxy   
  tcp        0      0 0.0.0.0:9000            0.0.0.0:*               LISTEN      3044/docker-proxy   
  tcp        0      0 0.0.0.0:3689            0.0.0.0:*               LISTEN      4006/rhythmbox      
  tcp6       0      0 :::80                   :::*                    LISTEN      2258/apache2        
  tcp6       0      0 :::21                   :::*                    LISTEN      2008/vsftpd         
  tcp6       0      0 :::22                   :::*                    LISTEN      2103/sshd: /usr/sbi 
  tcp6       0      0 ::1:3350                :::*                    LISTEN      2082/xrdp-sesman    
  tcp6       0      0 ::1:631                 :::*                    LISTEN      2532/cupsd          
  tcp6       0      0 :::8090                 :::*                    LISTEN      2258/apache2        
  tcp6       0      0 :::3389                 :::*                    LISTEN      2138/xrdp           
  tcp6       0      0 :::8000                 :::*                    LISTEN      3074/docker-proxy   
  tcp6       0      0 :::9000                 :::*                    LISTEN      3052/docker-proxy   
  tcp6       0      0 :::3689                 :::*                    LISTEN      4006/rhythmbox      
  udp        0      0 10.1.1.154:47776        0.0.0.0:*                           5188/firefox        
  udp        0      0 127.0.0.53:53           0.0.0.0:*                           1498/systemd-resolv 
  udp        0      0 172.17.0.1:49447        0.0.0.0:*                           7243/chrome         
  udp        0      0 0.0.0.0:631             0.0.0.0:*                           1960/cups-browsed   
  udp        0      0 0.0.0.0:37675           0.0.0.0:*                           1749/avahi-daemon:  
  udp        0      0 224.0.0.251:5353        0.0.0.0:*                           7243/chrome         
  udp        0      0 224.0.0.251:5353        0.0.0.0:*                           7243/chrome         
  udp        0      0 0.0.0.0:5353            0.0.0.0:*                           1749/avahi-daemon:  
  udp        0      0 10.1.1.158:46709        0.0.0.0:*                           7243/chrome         
  udp6       0      0 :::59863                :::*                                4131/app.asar --no- 
  udp6       0      0 :::51995                :::*                                1749/avahi-daemon:  
  udp6       0      0 :::5353                 :::*                                1749/avahi-daemon: `;
  let result = parseLinuxNetstat(commandResult)
  return new Promise(function(resolve,reject){
    setTimeout(()=>{
      resolve(result)
    },3000)
  })
}

export async function taskkill(pid) {
  console.log("kill task:"+pid)
  if(OS=='Linux') {
    return await killTaskLinux(pid)
  }
  else if(OS=='Windows') {
    return await killTaskWindows(pid)
  }
}
export async function parseNetstat(port) {
  
  
  if(OS=="Linux") {
    return await getLinuxNetStat(port)
  }
  else if(OS=='Windows') {
    return await getWindowsNetStat(port)
  }
  else {
    return await getH5NetStat(port)

  }
  // return result.filter(process=>process.localPort==port)

}
